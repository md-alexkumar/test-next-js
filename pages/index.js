import Head from "next/head";
import Image from "next/image";

import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Image src="/dhoni.webp" width={600} height={500} />

        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Alex blog!</a>
        </h1>
      </main>
    </div>
  );
}
